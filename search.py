# coding=UTF8
import os

W = '\033[0m'  # white (normal)
R = '\033[31m'  # red
G = '\033[32m'  # green
P = '\033[35m'  # purple

f_count = 0
d_count = 0
i_count = 0
f_list = []

d_in = input(G+"Directory path to search?  "+R)
u_in = input(G + "file name == " + R).lower()

print(R + "path     >>>   " + W + str(d_in))
print(R + "Input    >>>   " + W + str(u_in))
os.chdir(str(d_in))
for root, dirs, files in os.walk(".", topdown=True):
    for iname in files:
        lname = iname.lower()
        fname = os.path.splitext(lname)
        f_list.append(fname[0])
        f_count += 1
    for name in dirs:
        d_count += 1
for i, j in enumerate(f_list):
    if str(u_in) in str(j):
        print(P + "output   >>>   " + W + str(j))
        i_count += 1
    else:
        pass

print(str(i_count) + " " + "Files matching search")
print(str(f_count) + " " + "Files")
print(str(d_count) + " " + "Directories")
